# 实验3：创建分区表

## 实验目的

掌握分区表的创建方法，掌握各种分区方式的使用场景。


## 实验内容


- 本实验使用实验2的sale用户创建两张表：订单表(orders)与订单详表(order_details)。

- 两个表通过列order_id建立主外键关联。给表orders.customer_name增加B_Tree索引。

- 新建两个序列，分别设置orders.order_id和order_details.id，插入数据的时候，不需要手工设置这两个ID值。

- orders表按订单日期（order_date）设置范围分区。

- order_details表设置引用分区。

- 表创建成功后，插入数据，数据应该能并平均分布到各个分区。orders表的数据都大于40万行，order_details表的数据大于200万行（每个订单对应5个order_details）。

- 写出插入数据的脚本和两个表的联合查询的语句，并分析语句的执行计划。

- 进行分区与不分区的对比实验。

## 实验参考步骤

- 使用sql-developer软件创建表，并导出类似以下的脚本。

- 以下脚本不含orders.customer_name的索引，不含序列设置，仅供参考。



```python

CREATE TABLE orders 
(
 order_id NUMBER(9, 0) NOT NULL
 , customer_name VARCHAR2(40 BYTE) NOT NULL 
 , customer_tel VARCHAR2(40 BYTE) NOT NULL 
 , order_date DATE NOT NULL 
 , employee_id NUMBER(6, 0) NOT NULL 
 , discount NUMBER(8, 2) DEFAULT 0 
 , trade_receivable NUMBER(8, 2) DEFAULT 0 
 , CONSTRAINT ORDERS_PK PRIMARY KEY 
  (
    ORDER_ID 
  )
) 
TABLESPACE USERS 
PCTFREE 10 INITRANS 1 
STORAGE (   BUFFER_POOL DEFAULT ) 
NOCOMPRESS NOPARALLEL 

PARTITION BY RANGE (order_date) 
(
 PARTITION PARTITION_BEFORE_2016 VALUES LESS THAN (
 TO_DATE(' 2016-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
 'NLS_CALENDAR=GREGORIAN')) 
 NOLOGGING
 TABLESPACE USERS
 PCTFREE 10 
 INITRANS 1 
 STORAGE 
( 
 INITIAL 8388608 
 NEXT 1048576 
 MINEXTENTS 1 
 MAXEXTENTS UNLIMITED 
 BUFFER_POOL DEFAULT 
) 
NOCOMPRESS NO INMEMORY  
, PARTITION PARTITION_BEFORE_2020 VALUES LESS THAN (
TO_DATE(' 2020-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
'NLS_CALENDAR=GREGORIAN')) 
NOLOGGING
TABLESPACE USERS
, PARTITION PARTITION_BEFORE_2021 VALUES LESS THAN (
TO_DATE(' 2021-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
'NLS_CALENDAR=GREGORIAN')) 
NOLOGGING 
TABLESPACE USERS
);
--以后再逐年增加新年份的分区
ALTER TABLE orders ADD PARTITION partition_before_2022
VALUES LESS THAN(TO_DATE('2022-01-01','YYYY-MM-DD'))
TABLESPACE USERS;


```

![](11.png)

![](12.png)

![](13.png)

- 创建order_details表的语句如下：


```python
CREATE TABLE order_details
(
id NUMBER(9, 0) NOT NULL 
, order_id NUMBER(10, 0) NOT NULL
, product_id VARCHAR2(40 BYTE) NOT NULL 
, product_num NUMBER(8, 2) NOT NULL 
, product_price NUMBER(8, 2) NOT NULL 
, CONSTRAINT ORDER_DETAILS_PK PRIMARY KEY 
  (
    id 
  )
, CONSTRAINT order_details_fk1 FOREIGN KEY  (order_id)
REFERENCES orders  (  order_id   )
ENABLE
) 
TABLESPACE USERS 
PCTFREE 10 INITRANS 1 
STORAGE ( BUFFER_POOL DEFAULT ) 
NOCOMPRESS NOPARALLEL
PARTITION BY REFERENCE (order_details_fk1);

```

![](14.png)

- 创建序列SEQ1的语句如下


```python
CREATE SEQUENCE  SEQ1  MINVALUE 1 MAXVALUE 999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE  GLOBAL ;
```

![](15.png)

- 插入100条orders记录的样例脚本如下：


```python
declare 
   i integer;
   y integer;
   m integer;
   d integer;
   str varchar2(100);
BEGIN  
  i:=0;
  y:=2015;
  m:=1;
  d:=12;
  while i<100 loop
    i := i+1;
    --在这里改变y,m,d
    m:=m+1;
    if m>12 then
        m:=1;
    end if;
    str:=y||'-'||m||'-'||d;
    insert into orders(order_id,order_date) 
      values(SEQ1.nextval,to_date(str,'yyyy-MM-dd'));
  end loop;
  commit;
END;


```

![](16.png)


```python
DECLARE
  i INTEGER;
  j INTEGER;
  max_order_id NUMBER;
BEGIN
  SELECT MAX(order_id) INTO max_order_id FROM orders;
  i := max_order_id - 99;
  WHILE i <= max_order_id LOOP
    FOR j IN 1..5 LOOP
      INSERT INTO order_details (id, order_id, product_id, product_num, product_price)
      VALUES (SEQ1.nextval, i, 'product_' || j, j, j*100);
    END LOOP;
    i := i + 1;
  END LOOP;
  COMMIT;
END;

```

![](17.png)

## 查询


```python
SELECT o.order_id, o.customer_name, o.order_date, od.product_id, od.product_num, od.product_price
FROM orders o
INNER JOIN order_details od
ON o.order_id = od.order_id;
```

在这个例子中，我们使用了 INNER JOIN 操作符来联接 orders 和 order_details 两个表，并使用 ON 关键字指定了两个表之间关联的条件。我们选择了 orders 表和 order_details 表中的一些列，以用于结果集的显示。在这个查询中，我们以订单 ID 作为关联条件，这样我们可以将订单和订单明细数据联立起来，从而查看每个订单的详细信息。


![](19.png)

![](18.png)

## 查询语句执行计划分析


```python

PLAN_TABLE_OUTPUT       
---------------------------------------------------------------------------------------------------------------------
| Id  | Operation                           | Name          | Rows  | Bytes | Cost (%CPU)| Time     | Pstart| Pstop |   
|   0 | SELECT STATEMENT                    |               |     1 |   105 |   275   (0)| 00:00:01 |       |       |
|   1 |  NESTED LOOPS                       |               |     1 |   105 |   275   (0)| 00:00:01 |       |       |
|   2 |   NESTED LOOPS                      |               |     1 |   105 |   275   (0)| 00:00:01 |       |       |
|   3 |    PARTITION REFERENCE ALL          |               |     1 |    61 |   274   (0)| 00:00:01 |     1 |     4 |
|   4 |     TABLE ACCESS FULL               | ORDER_DETAILS |     1 |    61 |   274   (0)| 00:00:01 |     1 |     4 |
|*  5 |    INDEX UNIQUE SCAN                | ORDERS_PK     |     1 |       |     0   (0)| 00:00:01 |       |       |                                                                                  

```

在这个执行计划中，我们可以看到以下操作：

第0行：执行SELECT语句。

第1行：执行嵌套循环操作。

第2行：执行嵌套循环操作。

第3行：对orders表进行分区引用。

第4行：执行全表扫描操作，访问order_details表的所有分区。

第5行：执行唯一索引扫描操作，访问orders表的主键索引。

第6行：通过行ID访问orders表。

根据执行计划，可以看到主要的开销是在全表扫描order_details表上。因此，可以考虑对order_details表的查询添加WHERE子句或创建适当的索引来提高查询效率。

## 结论

1.分区表的创建可以提高查询性能，并且可以提高数据管理的效率。

2.分区方式的选择需要根据具体业务场景和数据量来确定，例如按时间范围分区可以适用于历史数据查询，按地域分区可以适用于多个区域的数据分离管理。

3.在查询大量数据时，使用索引可以提高查询性能。对于经常需要查询的字段，建议建立索引。

4.分区表与普通表的查询性能对比实验中，分区表的查询性能更优秀，特别是在大数据量查询时更为明显。


```python

```
