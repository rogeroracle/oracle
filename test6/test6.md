# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。

  表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
 
  设计权限及用户分配方案。至少两个用户。

  在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。

  设计一套数据库的备份方案。

## 期末考核要求

- 实验在自己的计算机上完成

- 文档必须提交到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件

     1.test6.md主文件
  
     2.数据库创建和维护用的脚本文件*.sql
  
     3.test6_design.docx，学校格式的完整报告


- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰

- 提交时间： 2023-5-26日前

# 总体设计

### 1.表空间和表格设计

设计两个表空间，分别为数据表空间和记录表空间，记作'DATA_TS'和'RECORD_TS'

设计五张表格，分别为商品表，意见反馈表，用户表，订单表，操作记录表。分别记作'Products','Advice','Orders','Users','OperateRD'

其中，商品表，用户表，订单表属于数据表空间，意见反馈表和操作记录表属于记录表空间。


```python
创建数据表空间（DATA_TS）：
CREATE TABLESPACE DATA_TS
   DATAFILE 'data_ts.dbf'
   SIZE 500M
   AUTOEXTEND ON
   NEXT 100M
   MAXSIZE UNLIMITED;
    
创建记录表空间（RECORD_TS）：
CREATE TABLESPACE RECORD_TS
   DATAFILE 'record_ts.dbf'
   SIZE 200M
   AUTOEXTEND ON
   NEXT 50M
   MAXSIZE UNLIMITED;
    
创建商品表（Products）：
CREATE TABLE Products (
   product_id NUMBER PRIMARY KEY,
   product_name VARCHAR2(100),
   price NUMBER(10, 2),
   quantity NUMBER
) TABLESPACE DATA_TS;

创建意见反馈表（Advice）：
CREATE TABLE Advice (
   advice_id NUMBER PRIMARY KEY,
   user_id NUMBER,
   feedback VARCHAR2(1000)
) TABLESPACE RECORD_TS;

创建用户表（Users）：
CREATE TABLE Users (
   user_id NUMBER PRIMARY KEY,
   username VARCHAR2(100),
   email VARCHAR2(100)
) TABLESPACE DATA_TS;

创建订单表（Orders）：
CREATE TABLE Orders (
   order_id NUMBER PRIMARY KEY,
   user_id NUMBER,
   product_id NUMBER,
   quantity NUMBER,
   order_date DATE
) TABLESPACE DATA_TS;

创建操作记录表（OperateRD）：
CREATE TABLE OperateRD (
   record_id NUMBER PRIMARY KEY,
   user_id NUMBER,
   action VARCHAR2(100),
   timestamp TIMESTAMP
) TABLESPACE RECORD_TS;
```

![](l1.png)

![](l2.png)

向每张表中各插入25000条数据

获取对表空间操作的权限

![](l5.png)


```python
-- 使用管理员角色登录数据库
-- ...

-- 向商品表（Products）插入25000条数据
INSERT INTO Products (product_id, product_name, price, quantity)
SELECT 
    LEVEL,
    'Product ' || LEVEL,
    ROUND(DBMS_RANDOM.VALUE(1, 1000), 2),
    ROUND(DBMS_RANDOM.VALUE(1, 100), 0)
FROM DUAL
CONNECT BY LEVEL <= 25000;

-- 向意见反馈表（Advice）插入25000条数据
INSERT INTO Advice (advice_id, user_id, feedback)
SELECT 
    LEVEL,
    ROUND(DBMS_RANDOM.VALUE(1, 100), 0),
    'Feedback ' || LEVEL
FROM DUAL
CONNECT BY LEVEL <= 25000;

-- 向用户表（Users）插入25000条数据
INSERT INTO Users (user_id, username, email)
SELECT 
    LEVEL,
    'User ' || LEVEL,
    'user' || LEVEL || '@example.com'
FROM DUAL
CONNECT BY LEVEL <= 25000;

-- 向订单表（Orders）插入25000条数据
INSERT INTO Orders (order_id, user_id, product_id, quantity, order_date)
SELECT 
    LEVEL,
    ROUND(DBMS_RANDOM.VALUE(1, 100), 0),
    ROUND(DBMS_RANDOM.VALUE(1, 25000), 0),
    ROUND(DBMS_RANDOM.VALUE(1, 10), 0),
    SYSDATE - ROUND(DBMS_RANDOM.VALUE(1, 365), 0)
FROM DUAL
CONNECT BY LEVEL <= 25000;

-- 向操作记录表（OperateRD）插入25000条数据
INSERT INTO OperateRD (record_id, user_id, action, timestamp)
SELECT 
    LEVEL,
    ROUND(DBMS_RANDOM.VALUE(1, 100), 0),
    'Action ' || LEVEL,
    SYSTIMESTAMP - INTERVAL '1' HOUR * ROUND(DBMS_RANDOM.VALUE(1, 10000), 0)
FROM DUAL
CONNECT BY LEVEL <= 25000;

```

![](l6.png)

提交事务，不然无法保存。（COMMIT）

![](l7.png)

查看表格，发现插入数据成功。

![](l8.png)

![](l9.png)

![](l10.png)

![](l11.png)

![](l12.png)

### 2.用户及权限分配设计

设计两个用户，分别为顾客以及管理员，其中用户能够对商品表进行查看，能够对意见反馈表进行增删查改，能够对订单表进行查看，管理员对五张表都能够进行增删查改。

首先，创建用户角色：


```python
-- 创建顾客角色
CREATE ROLE customer;

-- 创建管理员角色
CREATE ROLE administrator;
```

然后，为每个角色授予对应的权限：


```python
-- 顾客角色权限
GRANT SELECT ON Products TO customer;
GRANT SELECT ON Advice TO customer;
GRANT SELECT ON Orders TO customer;
GRANT INSERT, UPDATE, DELETE ON Advice TO customer;

-- 管理员角色权限
GRANT SELECT, INSERT, UPDATE, DELETE ON Products TO administrator;
GRANT SELECT, INSERT, UPDATE, DELETE ON Advice TO administrator;
GRANT SELECT, INSERT, UPDATE, DELETE ON Users TO administrator;
GRANT SELECT, INSERT, UPDATE, DELETE ON Orders TO administrator;
GRANT SELECT, INSERT, UPDATE, DELETE ON OperateRD TO administrator;

```

接下来，创建两个用户，并将相应的角色分配给它们：


```python
-- 创建顾客用户
CREATE USER customer_user IDENTIFIED BY password;
GRANT customer TO customer_user;

-- 创建管理员用户
CREATE USER admin_user IDENTIFIED BY password;
GRANT administrator TO admin_user;

```

### 3.PL/SQL语言设计

在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。

1.存储过程：处理订单（ProcessOrder）


```python
CREATE OR REPLACE PACKAGE BODY YourPackage AS

   PROCEDURE ProcessOrder(p_order_id IN NUMBER) IS
      v_product_id NUMBER;
      v_quantity NUMBER;
   BEGIN
      -- 根据订单ID获取订单信息
      SELECT product_id, quantity INTO v_product_id, v_quantity
      FROM Orders
      WHERE order_id = p_order_id;

      -- 执行业务逻辑
      -- 示例：更新商品表中的库存
      UPDATE Products
      SET quantity = quantity - v_quantity
      WHERE product_id = v_product_id;

      -- 其他业务逻辑
      -- ...

      COMMIT;
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         -- 处理找不到订单的情况
         DBMS_OUTPUT.PUT_LINE('Order not found.');
      WHEN OTHERS THEN
         -- 处理其他异常
         DBMS_OUTPUT.PUT_LINE('An error occurred during order processing.');
   END ProcessOrder;

END YourPackage;
/
```

2.获取商品价格（GetProductPrice）


```python
CREATE OR REPLACE PACKAGE BODY YourPackage AS

   FUNCTION GetProductPrice(p_product_id IN NUMBER) RETURN NUMBER IS
      v_price NUMBER;
   BEGIN
      -- 查询商品价格
      SELECT price INTO v_price
      FROM Products
      WHERE product_id = p_product_id;

      -- 返回价格
      RETURN v_price;
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         -- 处理找不到商品的情况
         DBMS_OUTPUT.PUT_LINE('Product not found.');
         RETURN NULL; -- 或返回适当的默认值
      WHEN OTHERS THEN
         -- 处理其他异常
         DBMS_OUTPUT.PUT_LINE('An error occurred while retrieving product price.');
         RETURN NULL; -- 或返回适当的默认值
   END GetProductPrice;

END YourPackage;
/

```

3.存储过程：添加意见反馈（AddFeedback）


```python
CREATE OR REPLACE PACKAGE BODY YourPackage AS

   PROCEDURE AddFeedback(
      p_advice_id IN NUMBER,
      p_user_id IN NUMBER,
      p_feedback IN VARCHAR2
   ) IS
   BEGIN
      -- 执行业务逻辑
      -- 示例：向意见反馈表中插入记录
      INSERT INTO Advice (advice_id, user_id, feedback)
      VALUES (p_advice_id, p_user_id, p_feedback);

      -- 其他业务逻辑
      -- ...

      COMMIT;
   EXCEPTION
      WHEN DUP_VAL_ON_INDEX THEN
         -- 处理主键冲突异常
         DBMS_OUTPUT.PUT_LINE('Feedback ID already exists.');
      WHEN OTHERS THEN
         -- 处理其他异常
         DBMS_OUTPUT.PUT_LINE('An error occurred while adding feedback.');
   END AddFeedback;

END YourPackage;
/

```

4.计算订单总金额（CalculateOrderTotal）


```python
CREATE OR REPLACE PACKAGE BODY YourPackage AS

   FUNCTION CalculateOrderTotal(p_order_id IN NUMBER) RETURN NUMBER IS
      v_total_amount NUMBER := 0;
   BEGIN
      -- 查询订单中的商品价格和数量，并计算总金额
      SELECT SUM(p.price * o.quantity)
      INTO v_total_amount
      FROM Orders o
      JOIN Products p ON o.product_id = p.product_id
      WHERE o.order_id = p_order_id;

      -- 返回总金额
      RETURN v_total_amount;
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         -- 处理找不到订单的情况
         DBMS_OUTPUT.PUT_LINE('Order not found.');
         RETURN NULL; -- 或返回适当的默认值
      WHEN OTHERS THEN
         -- 处理其他异常
         DBMS_OUTPUT.PUT_LINE('An error occurred while calculating order total.');
         RETURN NULL; -- 或返回适当的默认值
   END CalculateOrderTotal;

END YourPackage;
/

```

![](l13.png)

检验程序包是否能正常调用

![](l14.png)

![](l15.png)

![](l16.png)

### 4.设计备份方案

4.1备份方法 

使用 Oracle RMAN（Recovery Manager）进行备份：

配置 RMAN：在 Oracle 数据库服务器上，确保 RMAN 工具已正确配置。你可以创建一个 RMAN 配置文件，指定备份的目标设备和其他选项。

运行 RMAN：打开命令行界面，输入 rman 命令来启动 RMAN。你可以使用 RMAN 提供的命令来执行备份操作。

创建备份集：使用 backup 命令创建备份集。可以选择全备份（完整备份整个数据库）或增量备份（只备份发生更改的数据）。

指定备份目标：在备份命令中，指定备份集的目标位置，例如磁盘或磁带。你可以将备份保存到本地磁盘或远程存储位置。

运行备份：执行备份命令，等待备份过程完成。RMAN 将备份数据库文件并生成备份集。


```python
$ rman target /
RMAN> SHOW ALL;
RMAN> BACKUP DATABASE;
RMAN> LIST BACKUP;

```

4.2 备份策略

1.备份类别：

完整备份：每周日执行完整备份一次，备份整个数据库。

增量备份：每天从周一到周六执行增量备份，备份自上次完整备份以来的变更数据。

事务日志备份：每隔一定时间间隔（如每小时）执行事务日志备份，以便恢复到任意时间点。

2.备份介质：

磁盘存储：将备份文件存储在本地服务器的磁盘上，以提高备份和恢复的速度。

远程网络存储：将备份文件传输到远程服务器或云存储服务提供商，以提供灾难恢复和数据保护。

3.备份类型：

完整备份：每周日执行完整备份，备份整个数据库。

增量备份：每天执行增量备份，备份自上次完整备份以来的变更数据。

事务日志备份：定期执行事务日志备份，以便进行逐渐恢复或恢复到特定时间点。

4.保留期限：

保留最近的完整备份和增量备份文件，通常保留最近7天的备份文件。
保留一定时间范围内的事务日志备份文件，以便进行逐渐恢复或恢复到特定时间点。

测试和验证备份：

定期恢复备份文件到测试环境，并验证数据库的完整性和可用性。
定期检查备份文件的完整性和可恢复性，并记录检查结果。

5.灾难恢复：

制定灾难恢复策略，包括准备灾难恢复计划和团队，明确责任和流程。

建立冗余备份，将备份数据存储在不同的地理位置或云服务提供商中，以保证数据的可用性和持续性。

定期测试灾难恢复计划，包括从备份中恢复数据库并验证数据的完整性。

6.自动化备份过程：

使用自动化工具和脚本来执行备份操作，以减少人工错误和提高备份的一致性。

设置定时任务或调度程序，自动执行备份操作，并记录备份的状态和日志。

7.数据加密和安全性：

在备份过程中对备份文件进行加密，以保护敏感数据。

控制备份文件的访问权限，只有授权人员才能访问和恢复备份数据。

8.异地备份和云备份：

将备份数据复制到异地位置或云存储服务提供商，以提供灾难恢复和数据保护。

确保异地备份位置或云存储服务提供商具有高可用性和可靠性。
