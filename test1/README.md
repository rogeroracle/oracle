 ## 软件工程4班                       罗杰                     202010414411

 # 实验1：SQL语句的执行计划分析与优化指导

## 实验目的

分析SQL执行计划,执行SQL语句的优化指导,理解分析SQL语句的执行计划的重要作用。

## 实验数据库和用户

数据库是pdborcl，用户是sys和hr

## 实验内容

- 对Oracle12c中的HR人力资源管理系统中的表进行查询与分析。
- 设计自己的查询语句，并作相应的分析，查询语句不能太简单。执行两个比较复杂的返回相同查询结果数据集的SQL语句，通过分析SQL语句各自的执行计划，判断哪个SQL语句是最优的。最后将你认为最优的SQL语句通过sqldeveloper的优化指导工具进行优化指导，看看该工具有没有给出优化建议。

## 查询

- 用户hr默认没有统计权限，打开统计信息功能autotrace时要报错，必须要向用户hr授予以下视图的选择权限：

```text
 v_$sesstat, v_$statname 和 v_$session
```

- 权限分配过程如下

```sql
$ sqlplus sys/123@localhost/pdborcl as sysdba
@$ORACLE_HOME/sqlplus/admin/plustrce.sql
create role plustrace;
GRANT SELECT ON v_$sesstat TO plustrace;
GRANT SELECT ON v_$statname TO plustrace;
GRANT SELECT ON v_$mystat TO plustrace;
GRANT plustrace TO dba WITH ADMIN OPTION;
GRANT plustrace TO hr;
GRANT SELECT ON v_$sql TO hr;
GRANT SELECT ON v_$sql_plan TO hr;
GRANT SELECT ON v_$sql_plan_statistics_all TO hr;
GRANT SELECT ON v_$session TO hr;
GRANT SELECT ON v_$parameter TO hr; 
```

- 教材中的查询语句：查询两个国家('UK'和'US')各自的的部门的总数量，两个查询的结果是一样的。但效率不相同。

查询1：

```SQL

$sqlplus hr/123@localhost/pdborcl

set autotrace on

SELECT c.country_name, COUNT(d.department_id) as number_of_departments
FROM hr.countries c
JOIN locations l ON c.country_id = l.country_id
JOIN departments d ON l.location_id = d.location_id 
WHERE c.country_id IN ('UK', 'US')
GROUP BY c.country_name;

输出结果：
Predicate Information (identified by operation id):
---------------------------------------------------

   8 - access("L"."COUNTRY_ID"='UK' OR "L"."COUNTRY_ID"='US')
   9 - access("L"."LOCATION_ID"="D"."LOCATION_ID")
  10 - access("C"."COUNTRY_ID"="ITEM_1")
       filter("C"."COUNTRY_ID"='UK' OR "C"."COUNTRY_ID"='US')


统计信息
----------------------------------------------------------
	  4  recursive calls
	  0  db block gets
	 14  consistent gets
	  0  physical reads
	  0  redo size
	731  bytes sent via SQL*Net to client
	608  bytes received via SQL*Net from client
	  2  SQL*Net roundtrips to/from client
	  0  sorts (memory)
	  0  sorts (disk)
	  2  rows processed

分析

- 首先，对表 countries 进行全表扫描，筛选出 country_id 为 UK 或 US 的记录。
- 然后，对表 locations 进行关联查询操作，查找每个国家的对应的 location_id，并
选出符合条件的记录。
- 最后，对表 departments 进行关联查询操作，根据 location_id 选出符合条件的记
录，并进行 COUNT 聚合操作，统计每个国家的部门数量，并按照 country_name 分组。
```

- 查询2

```SQL
set autotrace on

SELECT c.country_name, 
       (SELECT COUNT(*) 
        FROM departments d 
        JOIN locations l ON d.location_id = l.location_id 
        WHERE l.country_id = c.country_id) AS number_of_departments
FROM countries c 
WHERE c.country_id IN ('UK', 'US');

输出结果：
Predicate Information (identified by operation id):
---------------------------------------------------

   4 - access("L"."COUNTRY_ID"=:B1)
   5 - access("D"."LOCATION_ID"="L"."LOCATION_ID")
   7 - access("C"."COUNTRY_ID"='UK' OR "C"."COUNTRY_ID"='US')


统计信息
----------------------------------------------------------
	  2  recursive calls
	  0  db block gets
	 12  consistent gets
	  0  physical reads
	  0  redo size
	731  bytes sent via SQL*Net to client
	608  bytes received via SQL*Net from client
	  2  SQL*Net roundtrips to/from client
	  0  sorts (memory)
	  0  sorts (disk)
	  2  rows processed

分析
----------------------------------------------------------
执行计划：

- 首先，对表 countries 进行全表扫描，筛选出 country_id 为 UK 或 US 的记录。
- 然后，对每个国家分别执行一个子查询，查询符合条件的部门数量。

```

## 最优查询与优化指导

- 根据执行计划可以看出，第一个查询使用了 JOIN 操作符，并对所有符合条件的表进行了关联查询操作，这可能会导致性能问题。相比之下，第二个查询仅仅对 departments 和 locations 表进行关联查询，并通过子查询仅计算了部门数量。因此，第二个查询更适合于这种场景，因为它的开销更小。因此，第二个 SQL 查询最优。

- sqldeveloper的优化指导工具没有给出优化建议，说明此查询已是最优查询。

## 实验结果分析

实现同一种查询结果可能有多种查询语句，但每种语句的执行效率不同，在进行查询时，应该尽量选择效率高的查询语句。同时sqlsqldeveloper的优化指导工具也会根据你的查询代码给出优化建议。

## 实验注意事项

- 完成时间：2023-3-21，请按时完成实验，过时扣分。
- 查询语句及分析文档必须提交到：你的oracle项目中的test1目录中。
- 实验分析及结果文档说明书用Markdown格式编写。


```python

```
